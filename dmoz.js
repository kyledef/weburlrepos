var cheerio = require('cheerio'),
	request = require('request'),
	fs = require('fs'),
	urls = [],
	categoryCount = [];
	masterRatio = 100,
	fullFilename = "links.json",
	base_url = "https://www.dmoz.org",
	exports = module.exports = {};


function processMainCategories(url, callback) {
	request(url, function (err, response, html) {
		if (!err) {
			var $ = cheerio.load(html);
			$('#catalogs span').each(function (i, el) {
				var category = $($(el).html()).attr('href');
				callback(category);
			});
		} else {
			console.log(err);
		}
	});
}

function getFileName(category){
	return category.replace(/\s+/g, '');
}

function saveLink(data) {
	var dataStr = JSON.stringify(data) + "," + "\n";
	var filename = "./data/"+getFileName(data.category)+".json";
	
	splitTrainTest(data.category, dataStr);

	fs.appendFile(filename, dataStr, function (err) { });
	fs.appendFile(fullFilename, dataStr, function (err) { });
}

function splitTrainTest(category, dataStr, ratio){
	var trainPath = "./train/",
		testPath = "./test/",
		filenameBase = getFileName(category);
	
	ratio = ratio || masterRatio;
	
	if (!categoryCount[category])categoryCount[category] = 1;
	else categoryCount[category]++;

	if (categoryCount[category] % ratio === 0){
		// Place in the validation file
		fs.appendFile(testPath + filenameBase + ".json", dataStr, function(err){ });
	}else{
		// Place in the training set
		fs.appendFile(trainPath + filenameBase + ".json", dataStr, function(err){ });
	}
}

function logError(err){
	console.log("Error Occurred: " + err);
	fs.appendFile("error.txt", err + "\n", function(err) { });
}

function processSubLinks(url, category, callback) {
	console.log(url);
	request(url, function (err, response, html) {
		if (!err && response.statusCode === 200 && html && html.length > 1) {
			try{
				var $ = cheerio.load(html);
				$(".directory-url li").each(function (i, a) {
					var expUlr = $($(a).html().trim()).attr('href');
					var obj = { "category": category, "url": expUlr, "path": url };
					callback(obj);
				});

				$('.dir-1').each(function (i, el) {
					$($(el).html()).attr('class', '').each(function (j, li) {
						$(li).children().each(function (i, ali) {
							var url2 = $($(ali).html().trim()).attr('href');
							if (url2 && url2.length > 2) {
								if (url2.indexOf("International") > 1) {
									console.log("skipping international links");
								} else if (urls.indexOf(url2) >= 0) {
									console.log("Accessed the following URL before: " + url2);
								}
								else {
									console.log(url2);
									urls.push(url2);
									processSubLinks(base_url + url2, category, callback);
								}
							}
						});

					});
				});
			}catch(errM){ logError(url + ":" + errM); }
		}else{
			logError(err);
			if (response)logError(url +":"+ response);
		}
	});
}


function main() {
	processMainCategories(base_url, function (categoryLink) {
		var category = categoryLink.substr(1, categoryLink.length - 2).toLowerCase();
		processSubLinks(base_url + categoryLink, category, saveLink);
	});
}

// Export Functionality
exports.processMainCategories = processMainCategories;
exports.processSubLinks = processSubLinks;
exports.getLinks = main;


// Checks if Module is called directly
if (require.main === module) main();