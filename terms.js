var dmoz = require('./dmoz'),
	cheerio = require('cheerio'),
	request = require('request'),
	base_url = "https://www.dmoz.org",
	fs = require('fs'),
	file_name = "terms.json",
	urls = [],
	depths = {},
	terms = {};



dmoz.processMainCategories(base_url, function (urlPath) {
	var category = urlPath.substr(1, urlPath.length - 2).toLowerCase();
	if (!depths[category]) depths[category] = 0;
	if (!terms[category]) terms[category] = [];

	depths[category] += 1;
	terms[category].push(category);
	
	processSubLinks(base_url + urlPath, category,saveTerms);
});

function saveTerms(category, terms){
	var data = { "category" : category, "terms" : terms };
	var dataStr = JSON.stringify(data) + ",\n";
	fs.appendFile(file_name, dataStr, function(err) {});
}

function processSubLinks(url, category, callback) {
	request(url, function (err, response, html) {
		if (!err){
			var $ = cheerio.load(html);
			
			$('.dir-1').each(function (i, el) {
				$($(el).html()).attr('class', '').each(function (j, li) {
					$(li).children().each(function (i, ali) {
						var url2 = $($(ali).html().trim()).attr('href');
						if (url2 && url2.indexOf("International") < 0 && urls.indexOf(url2) < 0){
							console.log(url2);
							urls.push(url2);
							var tokens = url2.split("/").filter(function(el){
								if (el.length > 1)return true;
							});
							callback(category, tokens);
							
							console.log("Depth: " + tokens.length + " " + "# URLs: " + urls.length); 
							if (tokens.length < 4){
								processSubLinks(base_url + url2, category, callback);
							}
						}
					});
				});
			});
		}
	});
}