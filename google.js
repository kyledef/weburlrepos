var google = require('google'), //https://github.com/jprichardson/node-google
	fs = require('fs'),
	filename = "links_google.json",
	terms = require("./terms.json"),
	maxCounter = 10;

google.resultsPerPage = 100;

terms.map(function (el) {
	el['search_terms'] = el['terms'].reduce(function (a, b) {
		return a + " " + b;
	});
});

terms.forEach(function (term) {
	console.log(term);
	var tCount = 0;
	google(term.search_terms, function (err, next, links) {
		if (!err) {
			links.forEach(function (el) {
				var data = { 'category': term.category, 'url': el.link };
				var dataStr = JSON.stringify(data) + ",\n";
				fs.appendFile(filename, dataStr, function (err) {
					if (err) console.log(err);
					console.log("Saved: " + el.title + " Link: " + el.link);
				});
			});
			if (tCount < maxCounter) {
				tCount += 1;
				if (next) next();
			}
		} else {
			console.log("error Occurred");
			console.dir(err);
		}
	});
});
	
