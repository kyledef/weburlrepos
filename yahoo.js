var cheerio = require('cheerio'),
	request = require('request'),
	fs = require('fs'),
	base_url = "https://business.yahoo.com";



function fromName2Category(name){
	return name.replace(/\s+/g, '_').toLowerCase();
}

function processMainCategories(url, callback){
	var categories = {};
	request(url, function(err, response, html){
		if (!err && response.statusCode === 200){
			var $ = cheerio.load(html);
			$(".maincat li a").each(function(i, el){
				console.log($(el).html());
				var categoryTxt = $(el).html();
				categories[categoryTxt] = {
					"category" : "",
					"original_category" : fromName2Category(categoryTxt)
				};
			});
			fs.appendFile("category.json",JSON.stringify(categories)+"\n", function(err){ } );
		}
	})
}


function main() {
	processMainCategories(base_url, function (categoryLink) {
		// var category = categoryLink.substr(1, categoryLink.length - 2).toLowerCase();
		// processSubLinks(base_url + categoryLink, category, saveLink);
	});
}

// Checks if Module is called directly
if (require.main === module) main();